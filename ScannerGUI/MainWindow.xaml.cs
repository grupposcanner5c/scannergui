﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScannerGUI
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    /// 
    //boh

    public partial class MainWindow : Window
    {

        private bool isModify;

        public MainWindow()
        {
            InitializeComponent();
            txtName.Clear();
            txtEmail.Clear();
            txtNtelefono.Clear();
            txtCognome.Clear();
            txtCitta.Clear();
            txtIndirizzo.Clear();
            txtDataRegistrazione.Clear();
            txtLeftEntrance.Text = 0.ToString();

            modifyTxtBox(false);

            this.isModify = false;
        }

        private void modifyTxtBox(bool enabled)
        {
            txtName.IsEnabled = enabled;
            txtEmail.IsEnabled = enabled;
            txtNtelefono.IsEnabled = enabled;
            txtCognome.IsEnabled = enabled;
            txtCitta.IsEnabled = enabled;
            txtIndirizzo.IsEnabled = enabled;
            txtDataRegistrazione.IsEnabled = enabled;
            txtLeftEntrance.IsEnabled = enabled;
        }

        private void btnModifica_Click(object sender, RoutedEventArgs e)
        {
            switch (isModify)
            {
                case true:
                    {
                        modifyTxtBox(false);
                        this.isModify = false;
                        ((Button)sender).Content = "Modifica";
                    }break;

                case false:
                    {
                        modifyTxtBox(true);
                        this.isModify = true;
                        ((Button)sender).Content = "Salva";
                    }
                    break;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnScansiona_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
